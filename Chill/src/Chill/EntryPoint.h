#pragma once

#ifdef CH_PLATFORM_WINDOWS


extern Chill::Application* Chill::CreateApplication();



int main(int argc, char **argv) {
	Chill::Log::Init();
	CH_CORE_WARN("Initialized Log!");
	int a = 17;
	CH_INFO("hello var = {0}", a);
	auto app = Chill::CreateApplication();
	app->Run();
	delete app;
}

#endif // CH_PLATFORM_WINDOWS
 