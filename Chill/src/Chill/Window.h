#pragma once

#include "chpch.h"
#include "Chill/Core.h"
#include "Chill/Events/Event.h"
#include "Chill/Log.h"


namespace Chill {
	struct WindowProps {
		std::string Titile;
		unsigned int Width;
		unsigned int Height;

		WindowProps(const std::string& title = "Chill now",
			unsigned int width = 1280,
			unsigned int height = 720) 
			: Titile(title), Width(width), Height(height)
		{}
	};
	class CHILL Window {
	public:
		using EventCallbackFn = std::function<void(Event&)>;
		
		virtual ~Window() {}
		
		virtual void OnUpdate() = 0;

		virtual unsigned int GetWidth() const = 0;
		virtual unsigned int GetHeight() const = 0;

		virtual void SetEventCallback(const EventCallbackFn& callback) = 0;
		virtual void SetVSync(bool enabled) = 0;
		virtual bool IsVSync() const = 0;

		static Window* Create(const WindowProps& props = WindowProps());
	};
}