#pragma once



#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

namespace Chill {

	class CHILL Log {
	public:
		static void Init();
		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() {
			return m_CoreLogger;
		}
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() {
			return m_ClientLogger;
		}

	private:
		static std::shared_ptr<spdlog::logger> m_CoreLogger;
		static std::shared_ptr<spdlog::logger> m_ClientLogger;
	};
}



#define CH_CORE_WARN(...)   Chill::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define CH_CORE_INFO(...)   Chill::Log::GetCoreLogger()->info(__VA_ARGS__)
#define CH_CORE_TRACE(...)  Chill::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define CH_CORE_FATAL(...)  Chill::Log::GetCoreLogger()->fatal(__VA_ARGS__)
#define CH_CORE_ERROR(...)  Chill::Log::GetCoreLogger()->error(__VA_ARGS__)


#define CH_ERROR(...)		Chill::Log::GetClientLogger()->error(__VA_ARGS__)
#define CH_WARN(...)		Chill::Log::GetClientLogger()->warn(__VA_ARGS__)
#define CH_INFO(...)		Chill::Log::GetClientLogger()->info(__VA_ARGS__)
#define CH_TRACE(...)		Chill::Log::GetClientLogger()->trace(__VA_ARGS__)
#define CH_FATAL(...)		Chill::Log::GetClientLogger()->fatal(__VA_ARGS__)
