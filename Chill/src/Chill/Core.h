#pragma once


#ifdef CH_PLATFORM_WINDOWS
	#ifdef CH_BUILD_DLL
		#define CHILL __declspec(dllexport)
	#else
		#define CHILL __declspec(dllimport)
	#endif // CH_BUILD_DLL
#else
	#error "Chill only support Windows."
#endif // CH_PLATFORM_WINDOWS

#ifdef CH_ENABLE_ASSERTS
	#define CH_ASSERT(x, ...) {if(!(x)) {CH_ERROR("Assertion Failed:{0}", __VA_ARGS__); __debugbreak();}}
	#define CH_CORE_ASSERT(x, ...) {if(!(x)){CH_CORE_ERROR("Assertion Failed:{0}",__VA_ARGS__);__debugbreak();}}
#else
	#define CH_ASSERT(x, ...)
	#define CH_CORE_ASSERT(x, ...)
#endif // CH_ENABLE_ASSERTS



#define BIT(x) (1 << x)