#pragma once

#include "Core.h"
#include "Events/Event.h"
#include "Chill/Window.h"

#include "Chill/Events/ApplicationEvent.h"



namespace Chill {

	class CHILL Application {
	public:
		Application();
		virtual ~Application();
		
		void Run();

		void OnEvent(Event& e);
	private:
		bool OnWindowClose(WindowCloseEvent& e);

		std::unique_ptr<Window> m_Window;
		bool m_Running = true;
	};

	Application* CreateApplication();
}

