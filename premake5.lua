workspace "Chill"
	architecture "x64"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["GLFW"] = "Chill/vendor/GLFW/include"

include "Chill/vendor/GLFW"

project "Chill"
	location "Chill"
	kind "SharedLib"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	pchheader "chpch.h"
	pchsource "Chill/src/chpch.cpp"
	
	files 
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"%{prj.name}/vendor/spdlog/include",
		"%{prj.name}/src",
		"%{IncludeDir.GLFW}"
	}

	links
	{
		"GLFW",
		"opengl32.lib"  
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines
		{
			"CH_PLATFORM_WINDOWS",
			"CH_BUILD_DLL",
		}
		
		postbuildcommands
		{
			("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox") 
		}

	filter "configurations:Debug"
		defines "CH_DEBUG"
		optimize "On"
	filter "configurations:Release"
		defines "CH_RELEASE"
		optimize "On"

	filter "configurations:Dist"
		defines "CH_DIST"
		optimize "On"



project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
	
	files 
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"Chill/vendor/spdlog/include",
		"Chill/src"
	}
	
	links
	{
		"Chill"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"
		defines
		{
			"CH_PLATFORM_WINDOWS"
		}
	
	filter "configurations:Debug"
		defines "CH_DEBUG"
		optimize "On"
	filter "configurations:Release"
		defines "CH_RELEASE"
		optimize "On"

	filter "configurations:Dist"
		defines "CH_DIST"
		optimize "On"

